BINARY = identifier

DEFS       += -DSTM32F1
FP_FLAGS   ?= -msoft-float
ARCH_FLAGS = -mthumb -mcpu=cortex-m3 $(FP_FLAGS) -mfix-cortex-m3-ldrd

PREFIX  ?= arm-none-eabi-
CC      := $(PREFIX)gcc
CXX     := $(PREFIX)g++
LD      := $(PREFIX)gcc
AR      := $(PREFIX)ar
AS      := $(PREFIX)as
OBJCOPY := $(PREFIX)objcopy
OBJDUMP := $(PREFIX)objdump
GDB     := $(PREFIX)gdb

OPENCM3_DIR ?= ./libopencm3
DEFS     += -I$(OPENCM3_DIR)/include
LDFLAGS  += -L$(OPENCM3_DIR)/lib
LIBNAME  = opencm3_stm32f1
LDLIBS   += -Wl,--start-group -lc -lgcc -lnosys -Wl,--end-group
LDLIBS   += -l$(LIBNAME)
LDSCRIPT ?= $(OPENCM3_DIR)/lib/stm32/f1/stm32f103x8.ld
LDFLAGS  += --static -nostartfiles
LDFLAGS  += -T$(LDSCRIPT)
LDFLAGS  += $(ARCH_FLAGS) $(DEBUG)

OPT    := -Os
DEBUG  := -ggdb3
CSTD   ?= -std=c99
CFLAGS += $(OPT) $(CSTD) $(DEBUG)
CFLAGS += $(ARCH_FLAGS)
CFLAGS += $(DEFS)
CFLAGS += -Wpedantic -Wall -Werror -Wundef -Wextra -Wshadow -Wredundant-decls -Wmissing-prototypes -Wstrict-prototypes -Wstrict-overflow=5
CFLAGS += -fno-common -ffunction-sections -fdata-sections

# SWD TAP ID (0x1ba01477 for STM32, 0x2ba01477 for most other clones)
CPUTAPID ?= 0x1ba01477
# serial port to flash using UART bootloader
PORT = /dev/ttyUSB0

OBJS += $(BINARY).o
GENERATED_BINARIES = $(BINARY).elf $(BINARY).bin $(BINARY).hex

all: elf bin hex

elf: $(BINARY).elf
bin: $(BINARY).bin
hex: $(BINARY).hex

$(OPENCM3_DIR)/lib/lib$(LIBNAME).a:
ifeq (,$(wildcard $@))
	$(warning $(LIBNAME).a not found, attempting to rebuild in $(OPENCM3_DIR))
	$(MAKE) -C $(OPENCM3_DIR)
endif

%.bin: %.elf
	$(OBJCOPY) -Obinary $(*).elf $(*).bin

%.hex: %.elf
	$(OBJCOPY) -Oihex $(*).elf $(*).hex

%.o: %.c
	$(CC) $(CFLAGS) -o $(*).o -c $(*).c

%.elf: $(OBJS) $(LDSCRIPT) $(OPENCM3_DIR)/lib/lib$(LIBNAME).a
	$(LD) $(LDFLAGS) $(OBJS) $(LDLIBS) -o $(*).elf
	size $(*).elf

flash_swd: $(BINARY).hex
	openocd --file interface/stlink.cfg --command 'transport select hla_swd' --command 'set CPUTAPID $(CPUTAPID)' --file target/stm32f1x.cfg --command 'adapter speed 100' --command 'init' --command 'halt' --command 'reset init' --command 'flash write_image erase $<' --command 'reset' --command 'shutdown'

flash: $(BINARY).hex
	stm32flash /dev/ttyUSB0 -w $< -R || stm32flash /dev/ttyUSB0 -w $< -R

debug: $(BINARY).elf
	 arm-none-eabi-gdb --eval-command='target remote | openocd --file interface/stlink.cfg --command "transport select hla_swd" --command "set CPUTAPID $(CPUTAPID)" --file target/stm32f1x.cfg --command "gdb_port pipe; log_output /dev/null; init"' $<

clean:
	$(RM) $(GENERATED_BINARIES) generated.* $(OBJS) $(OBJS:%.o=%.d)

.PHONY: clean elf bin hex

-include $(OBJS:.o=.d)
