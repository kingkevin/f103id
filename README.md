the f103id is a firmware for the STM32F103C micro-controller and alternatives (e.g. clones).
it will identify which silicon die it is actually running on.

flashing
========

the firmware to be flashed is `identifier.bin`.
the easiest way to program the firmware is using the embedded UART bootloader.
for that short the BOOT0 pin to VCC while powering up the device.
connect to the USART1 serial port.
then you can use `stm32flash` or simply run `make flash`.

operation
=========

connect to the USART1 serial port (USART1_TX is PA9 and USART1_RX is PA10) with the following setting: 115200 8N1.
type 'h' to list all available commands.
type 'a' to run all identification techniques.

capabilities
------------

the firmware can perform following checks:

- read unique ID
- read MCU ID
- read CPU ID
- read JEP ID
- read ROM table
- check flash size
- check SRAM size
- check peripheral
- check memory map
- check MCU ID errata

based on the check it can figure out:

- manufacturer
- chip family
- pin count
- most likely micro-controller

identification
--------------

it is able to identify following variants:

- STM32F103Cx (includes STM32F101Cx)
- CKS32F103Cx
- GD32F103Cx
- HK32F103Cx
- APM32F103Cx

it can also identify other variants, but this has not been tested.

I have not been able to operate the BLM32F103C8.

if you have tested it on other devices, please send the results (e.g. after running 'show all information') to f103id@cuvoodoo.info and I will integrated it in the list (and firmware).

compilation
===========

to compile the firmware yourself:

- install `arm-none-eabi-gcc` to compile the firmware.
- get [libopencm3](https://libopencm3.org/) using `git submodule update --init --recursive`.
- compile using `make`

to flash:

- install [stm32flash](https://sourceforge.net/projects/stm32flash/) to flash using the UARt bootlaoder
- flash using `make flash`

alternatively to flash:

- install [OpenOCD](http://openocd.org/) to flash using SWD (e.g. ST-LINK/V2 programmer)
- flash using `make flash_swd`
